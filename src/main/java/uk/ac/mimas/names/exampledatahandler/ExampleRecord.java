package uk.ac.mimas.names.exampledatahandler;
import java.util.ArrayList;
public class ExampleRecord {

	private String internalRecordID;
	private String familyNames;
	private String givenNames;
	private String institutionalAffiliation;
	private String fieldOfInterest;
	private String article;
	private ArrayList<String> collaborativeRelationships;
	
	public ExampleRecord(){
		this.collaborativeRelationships = new ArrayList<String>();
	}

	/**
	 * @return the internalRecordID
	 */
	public String getInternalRecordID() {
		return internalRecordID;
	}

	/**
	 * @param internalRecordID the internalRecordID to set
	 */
	public void setInternalRecordID(String internalRecordID) {
		this.internalRecordID = internalRecordID;
	}

	/**
	 * @return the familyNames
	 */
	public String getFamilyNames() {
		return familyNames;
	}

	/**
	 * @param familyNames the familyNames to set
	 */
	public void setFamilyNames(String familyNames) {
		this.familyNames = familyNames;
	}

	/**
	 * @return the givenNames
	 */
	public String getGivenNames() {
		return givenNames;
	}

	/**
	 * @param givenNames the givenNames to set
	 */
	public void setGivenNames(String givenNames) {
		this.givenNames = givenNames;
	}

	/**
	 * @return the institutionalAffiliation
	 */
	public String getInstitutionalAffiliation() {
		return institutionalAffiliation;
	}

	/**
	 * @param institutionalAffiliation the institutionalAffiliation to set
	 */
	public void setInstitutionalAffiliation(String institutionalAffiliation) {
		this.institutionalAffiliation = institutionalAffiliation;
	}

	/**
	 * @return the fieldOfInterest
	 */
	public String getFieldOfInterest() {
		return fieldOfInterest;
	}

	/**
	 * @param fieldOfInterest the fieldOfInterest to set
	 */
	public void setFieldOfInterest(String fieldOfInterest) {
		this.fieldOfInterest = fieldOfInterest;
	}

	/**
	 * @return the article
	 */
	public String getArticle() {
		return article;
	}

	/**
	 * @param article the article to set
	 */
	public void setArticle(String article) {
		this.article = article;
	}


	/**
	 * @param collaborator another example record that has a collaborative
	 *	relationship with this one.
	 */
	public void addCollaborativeRelationship(String collaboratorIdentifier){
		this.collaborativeRelationships.add(collaboratorIdentifier);
	}

	/**
	 * @return the collaborative relationships
	 */
	public ArrayList<String> getCollaborativeRelationships() {
		return collaborativeRelationships;
	}


	
	
}
