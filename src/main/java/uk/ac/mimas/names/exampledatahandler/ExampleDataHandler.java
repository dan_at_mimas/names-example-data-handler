package uk.ac.mimas.names.exampledatahandler;
import uk.ac.mimas.names.disambiguator.NamesDisambiguator;
import uk.ac.mimas.names.disambiguator.types.*;
import uk.ac.mimas.names.disambiguator.util.PersonNameParser;
import org.apache.log4j.Logger;
import org.apache.log4j.Level;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.io.FileOutputStream;
/**
 * 
 * @author danielneedham
 *
 */
public class ExampleDataHandler {
	private static final Logger logger = Logger.getLogger(ExampleDataHandler.class.getName());
	public static ArrayList<ExampleRecord> dataSource;
	public static int current = 0;
	public static int batchMax = 50;
	public static int procMax = 1000;
	public static String outputFile = "scores.txt";
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		logger.setLevel(Level.INFO);
		logger.info("Starting ExampleDataAnalyser.");
		try{
			initaliseDatasource();
			List<ExampleRecord> batch = new ArrayList<ExampleRecord>();
			// 1. Grab a batch of local records
			// here we will assume there is only a few and they
			// can be held in memory, however we could 
			// create a temporary database table to hold them 
			// in if necessary
			while(current <= procMax){
				// fill source with a new batch
				batch.clear();
				batch = getNextBatch();
				if(batch.size() == 0)
					break;
				
				// 2. Transform local records into Names records
				ArrayList<NormalisedRecord> transformedRecords = new ArrayList<NormalisedRecord>();
				for(ExampleRecord e : batch){
					transformedRecords.add(transformRecord(e));
				}
				batch.clear();
			
				
				// 3. Use NamesDisambiguator to deduplicate collection
				// We pass it a normalised batch and it returns the same 
				// batch with matchedness scores for each comparison record
				NamesDisambiguator disambiguator = new NamesDisambiguator();
				disambiguator.matchCollection(transformedRecords);
			
				// 4. do something with the response
				// we'll write the scores to a text file
				// Note: This assumes that each record has 
				// atleast one identifier assigned
				 Writer out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile, true), "UTF-8"));
				 
				 for(NormalisedRecord n: transformedRecords){
					 Map<NormalisedRecord, Double> map =  n.getMatchResults();
					 out.write(n.getNormalisedIdentifiers().get(0).getIdentifier());  							
					 out.write("\t");
					 for (Map.Entry<NormalisedRecord, Double> entry : map.entrySet()) {
							out.write(entry.getKey().getNormalisedIdentifiers().get(0).getIdentifier()  + " (" + entry.getValue() +")");
							out.write("\t");
					 }
					 out.write('\n');
				 }
				 
				 
	
			      
				out.close();
				
				current += batchMax;
				batchMax++;
			}
		}
		catch(Exception e){
			logger.error(e.toString());
			e.printStackTrace();
		}
		
		logger.info("Finished ExampleDataAnalyser.");
	}
	
	/**
	 * We used dataSource to simulate an external store of data. This could be a database, xml file, Application API etc..
	 * This method fills dataSource with example records.
	 */
	public static void initaliseDatasource(){
		dataSource = new ArrayList<ExampleRecord>();
		ExampleRecord e = new ExampleRecord();
		e.setInternalRecordID("#001");
		e.setGivenNames("Montgomery");
		e.setFamilyNames("Burns");
		e.setArticle("Accountancy in the Middle Ages.");
		e.setFieldOfInterest("Money");
		e.setInstitutionalAffiliation("Springfield University");
		dataSource.add(e);	
		e = new ExampleRecord();
		e.setInternalRecordID("#002");
		e.setGivenNames("Homer J.");
		e.setFamilyNames("Simpson");
		e.setFieldOfInterest("Donuts");
		e.setArticle("Nuclear Physics and thermo reactors.");
		e.setInstitutionalAffiliation("Springfield University");
		dataSource.add(e);	
		e = new ExampleRecord();
		e.setInternalRecordID("#003");
		e.setGivenNames("Bart");
		e.setFamilyNames("Simpson");
		e.setArticle("Eating shorts.");
		e.setFieldOfInterest("Skateboards");
		e.setInstitutionalAffiliation("Springfield University");
		dataSource.add(e);

		e = new ExampleRecord();
		e.setInternalRecordID("#004");
		e.setGivenNames("H. J.");
		e.setFamilyNames("Simpson");
		e.setArticle("Nuclear Physics, splitting the atom and thermo nuclear reactions.");
		e.setFieldOfInterest("Donuts");
		e.setInstitutionalAffiliation("Springfield University");
		dataSource.add(e);	
		
		e = new ExampleRecord();
		e.setInternalRecordID("#005");
		e.setGivenNames("B.");
		e.setFamilyNames("Simpson");
		e.setArticle("Short Eating.");
		e.setFieldOfInterest("Skateboards");
		e.setInstitutionalAffiliation("Springfield University");
		dataSource.add(e);	
		
		e = new ExampleRecord();
		e.setInternalRecordID("#006");
		e.setGivenNames("Homer");
		e.setFamilyNames("Simpson");
		e.setFieldOfInterest("Donuts");
		e.setArticle("Thermo reactors and physics, with some atoms included.");
		e.setInstitutionalAffiliation("Springfield University");
		dataSource.add(e);	
	}
	
	/**
	 * Returns a view of a portion of the data source arraylist.
	 * 
	 */
	
	public static List<ExampleRecord> getNextBatch(){
		int from = 0;
		int to = dataSource.size();
		
		from = current < 0 ? from : current;
		from = from < dataSource.size() ? from : dataSource.size();
		
		to = current + batchMax;
		to = to < 0 ? 0 : to;
		to = to < dataSource.size() ? to : dataSource.size();
		
		return dataSource.subList(from, to);
	}

	/**
	 * Finds an example record by its internal identifier
	 *
	 */
	public static ExampleRecord findByID(String identifier){
		for(ExampleRecord record : dataSource){
			if(record.getInternalRecordID().equalsIgnoreCase(identifier))
				return record;
		}
		return null;
	}


	/**
	 * Transforms an example record into a normalised record
	 * @param exampleRecord
	 * @return
	 */
	public static NormalisedRecord transformRecord(ExampleRecord exampleRecord){
		NormalisedRecord transformedRecord = new NormalisedRecord();
		transformedRecord.addNormalisedName(new NormalisedName(exampleRecord.getFamilyNames() + ", " + exampleRecord.getGivenNames()));
		PersonNameParser nameParser = new PersonNameParser();
		nameParser.parse(exampleRecord.getFamilyNames() + ", " + exampleRecord.getGivenNames());
		transformedRecord.getNormalisedTitles().addAll(nameParser.getTitles());
		transformedRecord.getNormalisedNames().get(0).getFamilyNames().addAll(nameParser.getFamilyNameComponents());
		transformedRecord.getNormalisedNames().get(0).getGivenNames().addAll(nameParser.getGivenNameComponents());
		transformedRecord.getNormalisedFieldsOfActivity().add(new NormalisedFieldOfActivity(exampleRecord.getFieldOfInterest()));
		transformedRecord.getNormalisedAffiliations().add(new NormalisedAffiliation(exampleRecord.getInstitutionalAffiliation()));
		NormalisedResultPublication article = new NormalisedResultPublication();
		article.setTitle(exampleRecord.getArticle());
		transformedRecord.addNormalisedResultPublication(article);
		NormalisedIdentifier identifier = new NormalisedIdentifier();
		identifier.setIdentifier(exampleRecord.getInternalRecordID());
		identifier.setBasisFor("EXAMPLE_SOURCE");
		transformedRecord.addNormalisedIdentifier(identifier);

		return transformedRecord;
	}

}
